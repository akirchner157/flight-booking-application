import psycopg2
from tkinter import *
from datetime import *
from copy import *


# Define all backend functions below
def add_user():
    try:
        name = login_name_entry.get()
        email = login_email_entry.get()
        password = login_password_entry.get()
        iata = login_iata_entry.get()
        idVal = 1
        exists = 0
        bad_iata = 0
        conn = psycopg2.connect(host="localhost", database="flights", user="flight", password="flight")
        cur = conn.cursor()

        sql = "SELECT email FROM customer WHERE email = %s"
        cur.execute(sql, (str(email), ))
        row = cur.fetchone()
        if row is not None:
            exists = 1

        #get ID value
        sql = "SELECT * FROM customer" 
        cur.execute(sql)
        row = cur.fetchone()
        while row is not None:
            idVal += 1
            row = cur.fetchone()

        #does IATA exist?
        sql = "SELECT * FROM airport WHERE iata = %s"
        cur.execute(sql, (str(iata), ))
        row = cur.fetchone()
        if row is None:
            bad_iata = 1
        else:    
            #Insert Values into Table
            sql = "INSERT INTO customer VALUES (%s,%s,%s,%s,%s)"
            cur.execute(sql, (str(idVal), str(name), str(email), str(password), str(iata)))
            conn.commit()

            #Get the page of the new account
            sql = "SELECT * FROM customer WHERE email = %s and password = %s"
            cur.execute(sql, (str(email), str(password)))
            row = cur.fetchone()
            rowTuple = row
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print("Database Connection Closed.")
        if exists == 1:
            display_login_error("User with email {} already exists".format(email))
        elif bad_iata == 1:
            display_login_error("IATA {} not valid".format(iata))
        else:
            global user_id, user_name
            user_id = rowTuple[0]
            user_name = rowTuple[1]
            profile_screen.lift()

def log_in_user():
    try:
        email = login_email_entry.get()
        password = login_password_entry.get()
        connect = 0
        conn = psycopg2.connect(host="localhost", database="flights", user="flight", password="flight")
        cur = conn.cursor()
        sql = "SELECT * FROM customer WHERE email = %s and password = %s"
        cur.execute(sql, (str(email), str(password)))
        row = cur.fetchone()
        if row is not None:
            connect = 1
        rowTuple = row
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print("Database Connection Closed.")
        if connect == 0:
            display_login_error("User with email {} does not exist".format(email))
        else:
            global user_id, user_name
            user_id = rowTuple[0]
            user_name = rowTuple[1]

            goto_flight_search_screen()

def submit_edited_address():
    try:
        address = edit_address_entry.get()
        conn = psycopg2.connect(host="localhost", database="flights", user="flight", password="flight")
        cur = conn.cursor()
        if current_address == "":
            sql = "INSERT INTO customer_address VALUES (%s, %s)"
            cur.execute(sql, (str(user_id), str(address)))
            conn.commit()
        else:
            sql = "UPDATE customer_address SET address = %s WHERE address = %s and c_id = %s"
            cur.execute(sql, (str(address), str(current_address), str(user_id)))
            conn.commit()
        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print("Database Connection Closed.")

        goto_profile_screen()

def submit_edited_cc():
    try:
        valid = True
        address = edit_cc_address_entry.get()
        number = int(edit_cc_number_entry.get())
        expiration = edit_cc_expiration_entry.get()
        name = edit_cc_name_entry.get()

        conn = psycopg2.connect(host="localhost", database="flights", user="flight", password="flight")
        cur = conn.cursor()
        if cc_number == "":
            sql = "SELECT * FROM customer_address WHERE address = %s"
            cur.execute(sql, (str(address), ))
            row = cur.fetchone()
            if row is None:
                valid = False
                display_edit_cc_error("Address is not valid")
            else:
                sql = "INSERT INTO credit_card VALUES (%s, %s, %s, %s, %s)"
                cur.execute(sql, (str(user_id), str(number), str(expiration), str(name), str(address)))
                conn.commit()
        else:
            if cc_number != number:
                sql = "UPDATE credit_card SET cc_number = %s WHERE cc_number = %s and c_id = %s"
                cur.execute(sql, (str(number), str(cc_number), str(user_id)))
                conn.commit()
            if cc_expiration != expiration:
                sql = "UPDATE credit_card SET expiration = %s WHERE cc_number = %s and c_id = %s"
                cur.execute(sql, (str(expiration), str(number), str(user_id)))
                conn.commit()
            if cc_name != name:
                sql = "UPDATE credit_card SET name = %s WHERE cc_number = %s and c_id = %s"
                cur.execute(sql, (str(name), str(number), str(user_id)))
                conn.commit()
            if cc_address != address:
                sql = "SELECT * FROM customer_address WHERE address = %s"
                cur.execute(sql, (str(address), ))
                conn.commit()
                row = cur.fetchone()
                if row is None:
                    valid = False
                    display_edit_cc_error("Address is not valid")
                else:
                    sql = "UPDATE credit_card SET address = %s WHERE cc_number = %s and c_id = %s"
                    cur.execute(sql, (str(address), str(number), str(user_id)))
                    conn.commit()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print("Database Connection Closed.")

        if valid:
            goto_profile_screen()

def submit_purchase(card_number, card_expiration, flights): 
    try:
        idVal = 1
        conn = psycopg2.connect(host="localhost", database="flights", user="flight", password="flight")
        cur = conn.cursor()

        #get ID value
        sql = "SELECT * FROM booking" 
        cur.execute(sql)
        row = cur.fetchone()
        while row is not None:
            idVal += 1
            row = cur.fetchone()
        x = 0
        for i in flights:
            sql = "INSERT INTO booking VALUES (%s,%s,%s,%s,%s,%s)"
            cur.execute(sql, (str(flights[x].code), str(flights[x].number), str(idVal), str(user_id), str(card_number), str(flights[x].first_class)))
            conn.commit()

            if flights[x].first_class == TRUE: #first class
                sql = "UPDATE flight SET filled_seats_fc = filled_seats_fc + 1 WHERE code = %s and number = %s"
                cur.execute(sql, (str(flights[x].code), str(flights[x].number)))
                conn.commit()
            else:
                sql = "UPDATE flight SET filled_seats_econ = filled_seats_econ + 1 WHERE code = %s and number = %s"
                cur.execute(sql, (str(flights[x].code), str(flights[x].number)))
                conn.commit()
            
            x += 1

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print("Database Connection Closed.")

        goto_flight_search_screen()

def get_cards():
    try:
        conn = psycopg2.connect(host="localhost", database="flights", user="flight", password="flight")
        cur = conn.cursor()

        sql = "SELECT cc_number, expiration, name, address FROM credit_card WHERE c_id = %s"
        cur.execute(sql, (str(user_id), ))
        row = cur.fetchone()
        cc = []
        while row is not None:
            cc.append(row)
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print("Database Connection Closed.")
            return cc
            

def get_addresses():
    try:
        conn = psycopg2.connect(host="localhost", database="flights", user="flight", password="flight")
        cur = conn.cursor()
        sql = "SELECT address FROM customer_address WHERE c_id = %s"
        cur.execute(sql, (str(user_id), ))
        row = cur.fetchone()
        addresses = []
        while row is not None:
            addresses.append(row)
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print("Database Connection Closed.")
            return addresses

def get_flights():
    try:
        conn = psycopg2.connect(host="localhost", database="flights", user="flight", password="flight")
        cur = conn.cursor()
        sql = "SELECT  flight.code, flight.number, airport_flight_origin.iata AS origin, airport_flight_destination.iata AS destination, airport_flight_origin.departure_time, airport_flight_destination.arrival_time, airport_origin.time_zone, airport_destination.time_zone, flight.price_econ, flight.price_fc, flight.capacity_econ, flight.capacity_fc, flight.filled_seats_econ, flight.filled_seats_fc"
        sql += " FROM flight NATURAL JOIN airport_flight_origin, airport_flight_destination, airport AS airport_origin, airport AS airport_destination"
        sql += " WHERE flight.number = airport_flight_destination.number and flight.code = airport_flight_destination.code"
        sql += " and airport_origin.iata = airport_flight_origin.iata and airport_destination.iata = airport_flight_destination.iata"
        cur.execute(sql)
        flights = []
        row = cur.fetchone()
        while row is not None:
            flight = Flight(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], None, row[10], row[11], row[12], row[13])
            flights.append(flight)
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print("Database Connection Closed.")
            return flights

def get_bookings():
    try:
        conn = psycopg2.connect(host="localhost", database="flights", user="flight", password="flight")
        cur = conn.cursor()
        sql = "SELECT flight.code, flight.number, booking.b_id, airport_flight_origin.iata AS origin, airport_flight_destination.iata AS destination, airport_flight_origin.departure_time, airport_flight_destination.arrival_time, booking.first_class,"
        sql += " airport_origin.time_zone, airport_destination.time_zone, flight.price_econ, flight.price_fc, flight.capacity_econ, flight.capacity_fc, flight.filled_seats_econ, flight.filled_seats_fc"
        sql += " FROM flight NATURAL JOIN airport_flight_origin, airport_flight_destination, booking, airport AS airport_origin, airport AS airport_destination"
        sql += " WHERE flight.number = airport_flight_destination.number and flight.code = airport_flight_destination.code"
        sql += " and flight.number = booking.number and flight.code = booking.code"
        sql += " and airport_origin.iata = airport_flight_origin.iata and airport_destination.iata = airport_flight_destination.iata"
        sql += " and booking.c_id = %s"
        cur.execute(sql, (str(user_id), ))
        row = cur.fetchone()
        bookings = []
        while row is not None:
            flight = Flight(row[0], row[1], row[3], row[4], row[5], row[6], row[8], row[9], row[10], row[11], row[7], row[12], row[13], row[14], row[15])
            row_added = 0
            x = 0
            for i in bookings:
                if bookings[x][0] == row[2]:
                    bookings[x].append(flight)
                    row_added = 1
                x += 1
            if row_added == 0:
                bookings.append([row[2], flight])
            row = cur.fetchone()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print("Database Connection Closed.")
            return bookings

def delete_address(address):
    try:
        conn = psycopg2.connect(host="localhost", database="flights", user="flight", password="flight")
        cur = conn.cursor()
        sql = "DELETE FROM customer_address WHERE c_id = %s and address = %s"
        cur.execute(sql, (str(user_id), str(address)))
        conn.commit()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print("Database Connection Closed.")

def delete_cc(number):
    try:
        conn = psycopg2.connect(host="localhost", database="flights", user="flight", password="flight")
        cur = conn.cursor()
        sql = "DELETE FROM credit_card WHERE c_id = %s and cc_number = %s"
        cur.execute(sql, (str(user_id), str(number)))
        conn.commit()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print("Database Connection Closed.")

def cancel_booking(booking_id):
    try:
        conn = psycopg2.connect(host="localhost", database="flights", user="flight", password="flight")
        cur = conn.cursor()
        sql = "SELECT code, number, first_class FROM booking WHERE b_id = %s and c_id = %s"
        cur.execute(sql, (str(booking_id), str(user_id)))
        row = cur.fetchone()
        while row is not None:
            if row[2] == TRUE: #first class
                sql = "UPDATE flight SET filled_seats_fc = filled_seats_fc - 1 WHERE code = %s and number = %s"
                cur.execute(sql, (str(row[0]), str(row[1])))
                conn.commit()
            else:
                sql = "UPDATE flight SET filled_seats_econ = filled_seats_econ - 1 WHERE code = %s and number = %s"
                cur.execute(sql, (str(row[0]), str(row[1])))
                conn.commit()
            try:
                row = cur.fetchone()
            except (Exception, psycopg2.DatabaseError) as error:
                #print(error)
                pass
            finally:
                break

        sql = "DELETE FROM booking WHERE b_id = %s and c_id = %s"
        cur.execute(sql, (str(booking_id), str(user_id)))
        conn.commit()

        cur.close()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    finally:
        if conn is not None:
            conn.close()
            print("Database Connection Closed.")

def find_bookings(flights, booking, origin, destination, departure_date, return_trip=False, return_date=None):
    if origin == destination:
        return [booking]
    bookings = []
    for flight in flights:
        if flight.origin == origin:
            if (flight.get_adjusted_departure_time().date() == departure_date or (flight.get_adjusted_departure_time() - timedelta(days=1)).date() == departure_date) and (len(booking.flights) == 0 or flight.departure_time > booking.flights[-1].arrival_time):
                valid_destination = True
                for previous_flight in booking.flights:
                    if flight.destination == previous_flight.origin:
                        valid_destination = False
                        break
                if valid_destination:
                    new_booking = deepcopy(booking)
                    new_booking.flights.append(flight)
                    bookings = bookings + find_bookings(flights, new_booking, flight.destination, destination, departure_date)
    if return_trip:
        final_bookings = []
        for original_booking in bookings:
            return_bookings = find_bookings(flights, Booking([], []), destination, origin, return_date)
            for return_booking in return_bookings:
                if original_booking.flights[-1].arrival_time < return_booking.flights[0].departure_time:
                    final_booking = deepcopy(original_booking)
                    final_booking.return_flights = return_booking.flights
                    final_bookings.append(final_booking)
        bookings = final_bookings
    return bookings

def filter_bookings(bookings, max_connections, max_time_hours, max_price):
    final_bookings = []
    for booking in bookings:
        valid = True
        if max_connections != '':
            if len(booking.flights) - 1 > int(max_connections) or len(booking.return_flights) - 1 > int(max_connections):
                valid = False

        if max_time_hours != '':
            if booking.get_duration() > timedelta(hours=int(max_time_hours)) or booking.get_return_duration() > timedelta(hours=int(max_time_hours)):
                valid = False

        if max_price != '':
            if booking.get_total_econ_price() > int(max_price) and booking.get_total_fc_price() > int(max_price):
                valid = False

        if valid:
            final_bookings.append(booking)

    return final_bookings

def sort_bookings(bookings, sort_parameter):
    if sort_parameter == 'price_fc':
        return sorted(bookings, key=lambda booking: booking.get_total_fc_price())
    elif sort_parameter == 'price_econ':
        return sorted(bookings, key=lambda booking: booking.get_total_econ_price())
    elif sort_parameter == 'time':
        return sorted(bookings, key=lambda booking: booking.get_duration() + booking.get_return_duration())

    return bookings

def search_flights():
    for slave in search_bookings_container.pack_slaves():
        slave.destroy()
    flights = get_flights()
    bookings = find_bookings(flights, Booking([], []), search_origin_entry.get(), search_destination_entry.get(), datetime.strptime(search_flight_date_entry.get(), '%Y-%m-%d').date(), search_return.get() == 1, datetime.strptime(search_return_date_entry.get(), '%Y-%m-%d').date() if search_return.get() == 1 else date.today())
    bookings = filter_bookings(bookings, search_filter_connections_entry.get(), search_filter_time_entry.get(), search_filter_price_entry.get())
    bookings = sort_bookings(bookings, search_sort.get())
    for booking in bookings:
        booking_container = SearchBooking(booking.flights, booking.return_flights)
        booking_container.pack()
        booking_container.init_frame(booking.get_total_fc_price(), booking.get_total_econ_price())


# Screen switching functions
def goto_edit_address_screen(address=''):
    global current_address
    current_address = address
    edit_address_entry.delete(0, END)
    edit_address_entry.insert(END, address)
    edit_address_screen.lift()

def goto_edit_cc_screen(number='', expiration='', name='', address=''):
    for slave in edit_cc_error_container.pack_slaves():
        slave.destroy()
    global cc_number, cc_expiration, cc_name, cc_address
    cc_number = number
    cc_expiration = expiration
    cc_name = name
    cc_address = address
    edit_cc_address_entry.delete(0, END)
    edit_cc_name_entry.delete(0, END)
    edit_cc_number_entry.delete(0, END)
    edit_cc_expiration_entry.delete(0, END)
    edit_cc_address_entry.insert(END, address)
    edit_cc_name_entry.insert(END, name)
    edit_cc_number_entry.insert(END, number)
    edit_cc_expiration_entry.insert(END, expiration)
    edit_cc_screen.lift()

def goto_flight_search_screen():
    flight_search_screen.lift()

def goto_profile_screen():
    for slave in profile_name_container.pack_slaves():
        slave.destroy()
    profile_name = Label(profile_name_container, text='Name: ' + user_name)
    profile_name.pack()
    for slave in profile_email_container.pack_slaves():
        slave.destroy()
    profile_email = Label(profile_email_container, text='Email: ' + login_email_entry.get())
    profile_email.pack()
    for slave in profile_address_container.pack_slaves():
        slave.destroy()
    addresses = get_addresses()
    for address in addresses:
        address_frame = ProfileAddress(address[0])
        address_frame.pack()
        address_frame.init_frame()

    for slave in profile_cc_container.pack_slaves():
        slave.destroy()
    cards = get_cards()
    for card in cards:
        card_frame = ProfileCard(card[0], card[1], card[2], card[3])
        card_frame.pack()
        card_frame.init_frame()

    profile_screen.lift()

def goto_bookings_screen():
    for slave in bookings_container.pack_slaves():
        slave.destroy()
    bookings = get_bookings()
    for booking in bookings:
        booking_frame = BookingsBooking(booking[0], booking[1:])
        booking_frame.pack()
        booking_frame.init_frame()

    bookings_screen.lift()

def goto_purchase_screen():
    for slave in purchase_card_container.pack_slaves():
        slave.destroy()
    cards = get_cards()
    for card in cards:
        card_frame = PurchaseCard(card[0], card[1], card[2])
        card_frame.pack()
        card_frame.init_frame()
    purchase_screen.lift()


# Global Flight object
class Flight:
    def __init__(self, code, number, origin, destination, departure_time, arrival_time, origin_offset, destination_offset, price_econ, price_fc, first_class, capacity_econ, capacity_fc, filled_seats_econ, filled_seats_fc):
        self.code = code
        self.number = number
        self.origin = origin
        self.destination = destination
        self.departure_time = departure_time
        self.arrival_time = arrival_time
        self.origin_offset = origin_offset
        self.destination_offset = destination_offset
        self.price_econ = price_econ
        self.price_fc = price_fc
        self.first_class = first_class
        self.capacity_econ = capacity_econ
        self.capacity_fc = capacity_fc
        self.filled_seats_econ = filled_seats_econ
        self.filled_seats_fc = filled_seats_fc
        self.departure_time.replace(tzinfo=timezone.utc)
        self.arrival_time.replace(tzinfo=timezone.utc)

    def get_adjusted_departure_time(self):
        return self.departure_time.astimezone(timezone(timedelta(hours=self.origin_offset)))

    def get_adjusted_arrival_time(self):
        return self.arrival_time.astimezone(timezone(timedelta(hours=self.destination_offset)))

    def get_duration(self):
        return self.arrival_time - self.departure_time


# Global Booking object
class Booking:
    def __init__(self, flights, return_flights):
        self.flights = flights
        self.return_flights = return_flights

    def get_duration(self):
        return self.flights[-1].arrival_time - self.flights[0].departure_time

    def get_total_fc_price(self):
        price_fc = 0
        for flight in self.flights + self.return_flights:
            price_fc += flight.price_fc
        return price_fc

    def get_total_econ_price(self):
        price_econ = 0
        for flight in self.flights + self.return_flights:
            price_econ += flight.price_econ
        return price_econ

    def get_return_duration(self):
        return self.return_flights[-1].arrival_time - self.return_flights[0].departure_time


# User Data
user_id = 0
user_name = ""
current_address = ""
cc_number = 0
cc_expiration = ""
cc_name = ""
cc_address = ""
current_booking = []

# Building the GUI
master = Tk()
master.title('Flight Booking Application')
master.wm_geometry("1000x1000")

# Screen initialization
mainframe = Frame(master)
login_screen = Frame(master)
profile_screen = Frame(master)
flight_search_screen = Frame(master)
purchase_screen = Frame(master)
edit_address_screen = Frame(master)
edit_cc_screen = Frame(master)
bookings_screen = Frame(master)

mainframe.pack(fill=BOTH, expand=TRUE)
login_screen.place(in_=mainframe, x=0, y=0, relwidth=1, relheight=1)
profile_screen.place(in_=mainframe, x=0, y=0, relwidth=1, relheight=1)
flight_search_screen.place(in_=mainframe, x=0, y=0, relwidth=1, relheight=1)
purchase_screen.place(in_=mainframe, x=0, y=0, relwidth=1, relheight=1)
edit_address_screen.place(in_=mainframe, x=0, y=0, relwidth=1, relheight=1)
edit_cc_screen.place(in_=mainframe, x=0, y=0, relwidth=1, relheight=1)
bookings_screen.place(in_=mainframe, x=0, y=0, relwidth=1, relheight=1)

# Login screen
login_container = Frame(login_screen)
login_container.pack()

def display_signup_info():
    empty_line = Label(login_container, text=' ')
    info = Label(login_container, text='Extra information for new accounts:')
    empty_line.grid(row=3, column=0)
    info.grid(row=4)
    login_name_label.grid(row=5, column=0)
    login_name_entry.grid(row=5, column=1)
    login_iata_label.grid(row=6, column=0)
    login_iata_entry.grid(row=6, column=1)
    login_submit_button.grid(row=7, column=1)

def display_login_error(error_message):
    login_error = Label(login_screen, text=error_message, fg='red')
    login_error.pack()


login_email_label = Label(login_container, text='Email Address: ')
login_email_entry = Entry(login_container)
login_password_label = Label(login_container, text='Password: ')
login_password_entry = Entry(login_container)
login_signup_button = Button(login_container, text='Sign Up', command=display_signup_info)
login_log_in_button = Button(login_container, text='Log In', command=log_in_user)
login_name_label = Label(login_container, text='Full Name: ')
login_name_entry = Entry(login_container)
login_iata_label = Label(login_container, text='Home Airport IATA Code (ex: \'JFK\'): ')
login_iata_entry = Entry(login_container)
login_submit_button = Button(login_container, text='Submit', command=add_user)

login_email_label.grid(row=0, column=0)
login_email_entry.grid(row=0, column=1)
login_password_label.grid(row=1, column=0)
login_password_entry.grid(row=1, column=1)
login_signup_button.grid(row=2, column=0)
login_log_in_button.grid(row=2, column=1)

# Edit address screen
edit_address_container = Frame(edit_address_screen)
edit_address_container.pack()

edit_address_label = Label(edit_address_container, text='New Address: ')
edit_address_entry = Entry(edit_address_container)
edit_address_submit = Button(edit_address_screen, text='Submit', command=submit_edited_address)

edit_address_label.pack(side=LEFT)
edit_address_entry.pack(side=LEFT)
edit_address_submit.pack()

# Edit credit card screen
edit_cc_container = Frame(edit_cc_screen)
edit_cc_container.pack()

def display_edit_cc_error(error_message):
    edit_cc_error = Label(edit_cc_error_container, text=error_message, fg='red')
    edit_cc_error.pack()


edit_cc_number_label = Label(edit_cc_container, text='Card Number: ')
edit_cc_number_entry = Entry(edit_cc_container)
edit_cc_expiration_label = Label(edit_cc_container, text='Expiration Date (MM/YY): ')
edit_cc_expiration_entry = Entry(edit_cc_container)
edit_cc_name_label = Label(edit_cc_container, text='Name on Card: ')
edit_cc_name_entry = Entry(edit_cc_container)
edit_cc_address_label = Label(edit_cc_container, text='Billing Address: ')
edit_cc_address_entry = Entry(edit_cc_container)
edit_cc_submit = Button(edit_cc_container, text='Submit', command=submit_edited_cc)
edit_cc_error_container = Frame(edit_cc_screen)

edit_cc_number_label.grid(row=0, column=0)
edit_cc_number_entry.grid(row=0, column=1)
edit_cc_expiration_label.grid(row=1, column=0)
edit_cc_expiration_entry.grid(row=1, column=1)
edit_cc_name_label.grid(row=2, column=0)
edit_cc_name_entry.grid(row=2, column=1)
edit_cc_address_label.grid(row=3, column=0)
edit_cc_address_entry.grid(row=3, column=1)
edit_cc_submit.grid(row=4, column=1)
edit_cc_error_container.pack()

# Profile screen
profile_container = Frame(profile_screen)
profile_container.pack()

class ProfileAddress(Frame):
    def __init__(self, address):
        Frame.__init__(self, profile_address_container, pady=10)
        self.address = address

    def init_frame(self):
        address = Label(self, text=self.address)
        edit_button = Button(self, text='Edit', command=self.edit_address)
        delete_button = Button(self, text='Delete', command=self.delete_address)
        address.grid(row=0, column=0)
        edit_button.grid(row=0, column=1)
        delete_button.grid(row=0, column=2)

    def edit_address(self):
        goto_edit_address_screen(self.address)

    def delete_address(self):
        delete_address(self.address)
        goto_profile_screen()
        self.destroy()

class ProfileCard(Frame):
    def __init__(self, number, expiration, name, address):
        Frame.__init__(self, profile_cc_container, pady=10)
        self.number = number
        self.expiration = expiration
        self.name = name
        self.address = address

    def init_frame(self):
        number = Label(self, text=str(self.number))
        edit_button = Button(self, text='Edit', command=self.edit_cc)
        delete_button = Button(self, text='Delete', command=self.delete_cc)
        number.grid(row=0, column=0)
        edit_button.grid(row=0, column=1)
        delete_button.grid(row=0, column=2)

    def edit_cc(self):
        goto_edit_cc_screen(self.number, self.expiration, self.name, self.address)

    def delete_cc(self):
        delete_cc(self.number)
        self.destroy()


profile_info_label = Label(profile_container, text='User Info:', pady=20)
profile_name_container = Frame(profile_container)
profile_email_container = Frame(profile_container)
profile_address_label = Label(profile_container, text='Addresses:')
profile_address_container = Frame(profile_container)
profile_address_add_button = Button(profile_container, text='Add', command=goto_edit_address_screen)
profile_cc_label = Label(profile_container, text='Credit Cards:')
profile_cc_container = Frame(profile_container)
profile_cc_add_button = Button(profile_container, text='Add', command=goto_edit_cc_screen)
profile_bookings_button = Button(profile_container, text='See Current Bookings', command=goto_bookings_screen)
profile_back_button = Button(profile_container, text='Back', command=goto_flight_search_screen)

profile_info_label.pack()
profile_name_container.pack()
profile_email_container.pack()
profile_address_label.pack()
profile_address_container.pack()
profile_address_add_button.pack()
profile_cc_label.pack()
profile_cc_container.pack()
profile_cc_add_button.pack()
profile_bookings_button.pack()
profile_back_button.pack()

# Flight search screen
search_container = Frame(flight_search_screen)
search_container.pack()

search_fc = IntVar()
search_econ = IntVar()
search_return = IntVar()
search_sort = StringVar()

class SearchBooking(Frame):
    def __init__(self, flights, return_flights):
        Frame.__init__(self, search_bookings_container, pady=20)
        self.flights = flights
        self.return_flights = return_flights

    def init_frame(self, fc_price, econ_price):
        flights_container = Frame(self)
        flights_container.pack()
        for flight in self.flights:
            flight_info = Label(flights_container, text='('+flight.get_adjusted_departure_time().ctime()+') '+flight.origin+' -> '+flight.destination+' ('+flight.get_adjusted_arrival_time().ctime()+') | First Class: $'+str(flight.price_fc)+' | Economy: $'+str(flight.price_econ))
            flight_info.pack()
        if len(self.return_flights) > 0:
            return_trip_label = Label(flights_container, text='Return Trip:')
            return_trip_label.pack()
            for flight in self.return_flights:
                flight_info = Label(flights_container, text='('+flight.get_adjusted_departure_time().ctime()+') '+flight.origin+' -> '+flight.destination+' ('+flight.get_adjusted_arrival_time().ctime()+') | First Class: $'+str(flight.price_fc)+' | Economy: $'+str(flight.price_econ))
                flight_info.pack()
        price_container = Frame(self)
        price_container.pack()

        fc_label = Label(price_container, text='First Class: $'+str(fc_price))
        econ_label = Label(price_container, text='Economy: $'+str(econ_price))
        fc_button = Button(price_container, text='Book', command=self.purchase_fc)
        econ_button = Button(price_container, text='Book', command=self.purchase_econ)

        if search_fc.get() == 1:
            fc_label.grid(row=0, column=0)
            fc_button.grid(row=1, column=0)
            if search_econ.get() == 1:
                econ_label.grid(row=0, column=1)
                econ_button.grid(row=1, column=1)
        elif search_econ.get() == 1:
            econ_label.grid(row=0, column=0)
            econ_button.grid(row=1, column=0)

    def purchase_fc(self):
        global current_booking
        total_flights = self.flights + self.return_flights
        for flight in total_flights:
            flight.first_class = True
        current_booking = total_flights
        goto_purchase_screen()

    def purchase_econ(self):
        global current_booking
        total_flights = self.flights + self.return_flights
        for flight in total_flights:
            flight.first_class = False
        current_booking = total_flights
        goto_purchase_screen()


search_top_line = Frame(search_container)
search_origin_label = Label(search_top_line, text='Origin (Airport Code): ')
search_origin_entry = Entry(search_top_line)
search_destination_label = Label(search_top_line, text='Destination (Airport Code): ')
search_destination_entry = Entry(search_top_line)
search_submit_button = Button(search_top_line, text='Search', command=search_flights)
search_second_line = Frame(search_container)
search_flight_date_label = Label(search_second_line, text='Date (YYYY-MM-DD): ')
search_flight_date_entry = Entry(search_second_line)
search_return_check = Checkbutton(search_second_line, text='Include Return Trip', variable=search_return)
search_return_date_label = Label(search_second_line, text='Date (YYYY-MM-DD): ')
search_return_date_entry = Entry(search_second_line)
search_options_container = Frame(search_container)
search_sort_container = Frame(search_options_container)
search_filter_container = Frame(search_options_container)
search_checks_container = Frame(search_options_container)
search_fc_check = Checkbutton(search_checks_container, text='First Class', variable=search_fc)
search_econ_check = Checkbutton(search_checks_container, text='Economy', variable=search_econ)
search_sort_label = Label(search_sort_container, text='Sort By:')
search_sort_price_econ_radio = Radiobutton(search_sort_container, text='Economy Price', variable=search_sort, value='price_econ')
search_sort_price_fc_radio = Radiobutton(search_sort_container, text='First Class Price', variable=search_sort, value='price_fc')
search_sort_time_radio = Radiobutton(search_sort_container, text='Total Trip Time', variable=search_sort, value='time')
search_filter_label = Label(search_filter_container, text='Filter By:')
search_filter_connections_label = Label(search_filter_container, text='Number of connections: ')
search_filter_connections_entry = Entry(search_filter_container)
search_filter_time_label = Label(search_filter_container, text='Trip time (hours): ')
search_filter_time_entry = Entry(search_filter_container)
search_filter_price_label = Label(search_filter_container, text='Price (dollars): ')
search_filter_price_entry = Entry(search_filter_container)
search_profile_button = Button(flight_search_screen, text='Profile', command=goto_profile_screen)
search_bookings_container = Frame(search_container)
search_bookings = []

search_top_line.pack()
search_origin_label.pack(side=LEFT)
search_origin_entry.pack(side=LEFT)
search_destination_label.pack(side=LEFT)
search_destination_entry.pack(side=LEFT)
search_submit_button.pack(side=LEFT)
search_second_line.pack()
search_flight_date_label.pack(side=LEFT)
search_flight_date_entry.pack(side=LEFT)
search_return_check.pack(side=LEFT)
search_return_date_label.pack(side=LEFT)
search_return_date_entry.pack(side=LEFT)
search_options_container.pack()
search_checks_container.grid(row=0, column=0)
search_sort_container.grid(row=0, column=1)
search_filter_container.grid(row=0, column=2)
search_fc_check.pack()
search_econ_check.pack()
search_sort_label.pack()
search_sort_price_econ_radio.pack()
search_sort_price_fc_radio.pack()
search_sort_time_radio.pack()
search_filter_label.grid(row=0, column=0)
search_filter_connections_label.grid(row=1, column=0)
search_filter_connections_entry.grid(row=1, column=1)
search_filter_time_label.grid(row=2, column=0)
search_filter_time_entry.grid(row=2, column=1)
search_filter_price_label.grid(row=3, column=0)
search_filter_price_entry.grid(row=3, column=1)
search_bookings_container.pack()
search_profile_button.place(rely=0.0, relx=1.0, x=0, y=0, anchor=NE)

# Purchase screen
purchase_container = Frame(purchase_screen)
purchase_container.pack()

class PurchaseCard(Frame):
    def __init__(self, number, expiration, name):
        Frame.__init__(self, purchase_card_container)
        self.number = number
        self.expiration = expiration
        self.name = name

    def init_frame(self):
        number = Label(self, text='Number: ' + str(self.number))
        expiration = Label(self, text='Expires: ' + self.expiration)
        name = Label(self, text=self.name)
        button = Button(self, text='Use', command=self.use_card)
        number.grid(row=0, column=0)
        expiration.grid(row=0, column=1)
        name.grid(row=1, column=0)
        button.grid(row=1, column=1)

    def use_card(self):
        submit_purchase(self.number, self.expiration, current_booking)


purchase_prompt = Label(purchase_container, text='Which credit card do you want to use?')
purchase_card_container = Frame(purchase_container)
purchase_back_button = Button(purchase_container, text='Back', command=goto_flight_search_screen())

purchase_prompt.pack()
purchase_card_container.pack()
purchase_back_button.pack()

# Bookings screen
bookings_container = Frame(bookings_screen)
bookings_container.pack()

class BookingsBooking(Frame):
    def __init__(self, booking_id, flights):
        Frame.__init__(self, bookings_container, pady=20)
        self.booking_id = booking_id
        self.flights = flights

    def init_frame(self):
        container = Frame(self)
        container.grid(row=0, column=0)
        for flight in self.flights:
            flight_info = Label(container, text=flight.get_adjusted_departure_time().ctime()+' '+flight.origin+' -> '+flight.destination+' '+flight.get_adjusted_arrival_time().ctime())
            flight_info.pack()
        cancel = Button(self, text='Cancel Booking', command=self.cancel_booking)
        cancel.grid(row=0, column=1)

    def cancel_booking(self):
        cancel_booking(self.booking_id)
        self.destroy()


bookings_back_button = Button(bookings_screen, text='Back', command=goto_profile_screen)
bookings_back_button.pack()

login_screen.lift()  # Initializes the login page as the first screen
master.mainloop()  # This has to end the file as it locks the program in an infinite loop until the window is closed
