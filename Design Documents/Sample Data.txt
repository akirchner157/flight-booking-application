INSERT INTO airport VALUES ('ORD', 'Ohare International Airport', 'United States', 'Illinois', -6);
INSERT INTO airport VALUES ('LAX', 'Los Angeles International Airport', 'United States', 'California', -8);
INSERT INTO airport VALUES ('DCA', 'Ronald Reagan Washington National Airport', 'United States', 'Virginia', -5);
INSERT INTO airport VALUES ('LHR', 'Heathrow Airport', 'United Kingdom', null, 0);
INSERT INTO airport VALUES ('ICN', 'Incheon International Airport', 'South Korea', null, 9);


INSERT INTO airline VALUES ('AA', 'American Airlnes', 'United States');
INSERT INTO airline VALUES ('VS', 'Virgin Atlantic', 'England');
INSERT INTO airline VALUES ('KE', 'Korean Air', 'South Korea');


INSERT INTO customer VALUES (1, 'Adrian Kirchner', 'akirchner@hawk.iit.edu', 'password', 'ORD');
INSERT INTO customer VALUES (2, 'Alexander Bolejack', 'abolejack@hawk.iit.edu', 'wordpass', 'ICN');
INSERT INTO customer VALUES (3, 'John Doe', 'jDoe@hawk.iit.edu', 'password1', 'DCA');


INSERT INTO customer_address VALUES (1, '1234 road st, St. Paul, MN, 99999');
INSERT INTO customer_address VALUES (2, '1234 road st, Detroit, MI, 99999');
INSERT INTO customer_address VALUES (2, '1234 road st, Birmingham, MI, 99999');
INSERT INTO customer_address VALUES (3, '1234 road st, Washington, DC, 99999');


INSERT INTO credit_card VALUES (1, 123456789123456, '12/21', 'Adrian Kirchner', '1234 road st, St. Paul, MN, 99999');
INSERT INTO credit_card VALUES (2, 1234567891234567, '12/24', 'Alexander Bolejack', '1234 road st, Detroit, MI, 99999');
INSERT INTO credit_card VALUES (2, 7654321987654321, '06/21', 'Alexander Bolejack', '1234 road st, Detroit, MI, 99999');
INSERT INTO credit_card VALUES (3, 1234567123456789, '05/22', 'John E Doe', '1234 road st, Washington, DC, 99999');


INSERT INTO mileage VALUES ('AA', 1, 500);
INSERT INTO mileage VALUES ('VS', 1, 1500);
INSERT INTO mileage VALUES ('KE', 2, 5000);


INSERT INTO flight VALUES ('KE', 200, 10, 100, 0, 0, 6533.20, 653.99, 6533);
INSERT INTO airport_flight_origin VALUES ('KE', 200, 'ICN', '2019-11-26 1:30:30.00');
INSERT INTO airport_flight_destination VALUES ('KE', 200, 'ORD', '2019-11-26 2:30:30.00');
INSERT INTO flight VALUES ('KE', 201, 10, 100, 0, 0, 6533.20, 653.99, 6533);
INSERT INTO airport_flight_origin VALUES ('KE', 201, 'ORD', '2019-11-27 9:00:30.00');
INSERT INTO airport_flight_destination VALUES ('KE', 201, 'ICN', '2019-11-27 10:00:30.00');

INSERT INTO flight VALUES ('KE', 300, 10, 100, 0, 0, 5978.20, 597.99, 5978);
INSERT INTO airport_flight_origin VALUES ('KE', 300, 'ICN', '2019-11-26 3:00:30.00');
INSERT INTO airport_flight_destination VALUES ('KE', 300, 'LAX', '2019-11-26 4:00:30.00');
INSERT INTO flight VALUES ('KE', 301, 10, 100, 0, 0, 5978.20, 597.99, 5978);
INSERT INTO airport_flight_origin VALUES ('KE', 301, 'LAX', '2019-11-27 7:30:30.00');
INSERT INTO airport_flight_destination VALUES ('KE', 301, 'ICN', '2019-11-27 8:30:30.00');

INSERT INTO flight VALUES ('AA', 400, 10, 100, 0, 0, 2026.20, 202.99, 2026);
INSERT INTO airport_flight_origin VALUES ('AA', 400, 'LAX', '2019-11-26 4:30:30.00');
INSERT INTO airport_flight_destination VALUES ('AA', 400, 'ORD', '2019-11-26 5:30:30.00');
INSERT INTO flight VALUES ('AA', 401, 10, 100, 0, 0, 2026.20, 202.99, 2026);
INSERT INTO airport_flight_origin VALUES ('AA', 401, 'ORD', '2019-11-27 6:00:30.00');
INSERT INTO airport_flight_destination VALUES ('AA', 401, 'LAX', '2019-11-27 7:00:30.00');

INSERT INTO flight VALUES ('AA', 500, 10, 100, 0, 0, 726.20, 72.99, 726);
INSERT INTO airport_flight_origin VALUES ('AA', 500, 'ORD', '2019-11-26 6:00:30.00');
INSERT INTO airport_flight_destination VALUES ('AA', 500, 'DCA', '2019-11-26 7:00:30.00');
INSERT INTO flight VALUES ('AA', 501, 10, 100, 0, 0, 726.20, 72.99, 726);
INSERT INTO airport_flight_origin VALUES ('AA', 501, 'DCA', '2019-11-27 4:30:30.00');
INSERT INTO airport_flight_destination VALUES ('AA', 501, 'ORD', '2019-11-27 5:30:30.00');

INSERT INTO flight VALUES ('VS', 600, 10, 100, 0, 0, 3940.20, 394.99, 3940);
INSERT INTO airport_flight_origin VALUES ('VS', 600, 'ORD', '2019-11-26 7:30:30.00');
INSERT INTO airport_flight_destination VALUES ('VS', 600, 'LHR', '2019-11-26 8:30:30.00');
INSERT INTO flight VALUES ('VS', 601, 10, 100, 0, 0, 3940.20, 394.99, 3940);
INSERT INTO airport_flight_origin VALUES ('VS', 601, 'LHR', '2019-11-27 3:00:30.00');
INSERT INTO airport_flight_destination VALUES ('VS', 601, 'ORD', '2019-11-27 4:00:30.00');

INSERT INTO flight VALUES ('VS', 700, 10, 100, 0, 0, 5503.20, 550.99, 5503);
INSERT INTO airport_flight_origin VALUES ('VS', 700, 'LHR', '2019-11-26 9:00:30.00');
INSERT INTO airport_flight_destination VALUES ('VS', 700, 'ICN', '2019-11-26 10:00:30.00');
INSERT INTO flight VALUES ('VS', 701, 10, 100, 0, 0, 5503.20, 550.99, 5503);
INSERT INTO airport_flight_origin VALUES ('VS', 701, 'LHR', '2019-11-27 1:30:30.00');
INSERT INTO airport_flight_destination VALUES ('VS', 701, 'ICN', '2019-11-27 2:30:30.00');

INSERT INTO flight VALUES ('KE', 202, 10, 100, 0, 0, 6533.20, 653.99, 6533);
INSERT INTO airport_flight_origin VALUES ('KE', 202, 'ORD', '2019-11-26 1:30:30.00');
INSERT INTO airport_flight_destination VALUES ('KE', 202, 'ICN', '2019-11-26 2:30:30.00');
INSERT INTO flight VALUES ('KE', 203, 10, 100, 0, 0, 6533.20, 653.99, 6533);
INSERT INTO airport_flight_origin VALUES ('KE', 203, 'ICN', '2019-11-27 9:00:30.00');
INSERT INTO airport_flight_destination VALUES ('KE', 203, 'ORD', '2019-11-27 10:00:30.00');

INSERT INTO flight VALUES ('KE', 302, 10, 100, 0, 0, 5978.20, 597.99, 5978);
INSERT INTO airport_flight_origin VALUES ('KE', 302, 'LAX', '2019-11-26 3:00:30.00');
INSERT INTO airport_flight_destination VALUES ('KE', 302, 'ICN', '2019-11-26 4:00:30.00');
INSERT INTO flight VALUES ('KE', 303, 10, 100, 0, 0, 5978.20, 597.99, 5978);
INSERT INTO airport_flight_origin VALUES ('KE', 303, 'ICN', '2019-11-27 7:30:30.00');
INSERT INTO airport_flight_destination VALUES ('KE', 303, 'LAX', '2019-11-27 8:30:30.00');

INSERT INTO flight VALUES ('AA', 402, 10, 100, 0, 0, 2026.20, 202.99, 2026);
INSERT INTO airport_flight_origin VALUES ('AA', 402, 'ORD', '2019-11-26 4:30:30.00');
INSERT INTO airport_flight_destination VALUES ('AA', 402, 'LAX', '2019-11-26 5:30:30.00');
INSERT INTO flight VALUES ('AA', 403, 10, 100, 0, 0, 2026.20, 202.99, 2026);
INSERT INTO airport_flight_origin VALUES ('AA', 403, 'LAX', '2019-11-27 6:00:30.00');
INSERT INTO airport_flight_destination VALUES ('AA', 403, 'ORD', '2019-11-27 7:00:30.00');

INSERT INTO flight VALUES ('AA', 502, 10, 100, 0, 0, 726.20, 72.99, 726);
INSERT INTO airport_flight_origin VALUES ('AA', 502, 'DCA', '2019-11-26 6:00:30.00');
INSERT INTO airport_flight_destination VALUES ('AA', 502, 'ORD', '2019-11-26 7:00:30.00');
INSERT INTO flight VALUES ('AA', 503, 10, 100, 0, 0, 726.20, 72.99, 726);
INSERT INTO airport_flight_origin VALUES ('AA', 503, 'ORD', '2019-11-27 4:30:30.00');
INSERT INTO airport_flight_destination VALUES ('AA', 503, 'DCA', '2019-11-27 5:30:30.00');

INSERT INTO flight VALUES ('VS', 602, 10, 100, 0, 0, 3940.20, 394.99, 3940);
INSERT INTO airport_flight_origin VALUES ('VS', 602, 'LHR', '2019-11-26 7:30:30.00');
INSERT INTO airport_flight_destination VALUES ('VS', 602, 'ORD', '2019-11-26 8:30:30.00');
INSERT INTO flight VALUES ('VS', 603, 10, 100, 0, 0, 3940.20, 394.99, 3940);
INSERT INTO airport_flight_origin VALUES ('VS', 603, 'ORD', '2019-11-27 3:00:30.00');
INSERT INTO airport_flight_destination VALUES ('VS', 603, 'LHR', '2019-11-27 4:00:30.00');

INSERT INTO flight VALUES ('VS', 702, 10, 100, 0, 0, 5503.20, 550.99, 5503);
INSERT INTO airport_flight_origin VALUES ('VS', 702, 'ICN', '2019-11-26 9:00:30.00');
INSERT INTO airport_flight_destination VALUES ('VS', 702, 'LHR', '2019-11-26 10:00:30.00');
INSERT INTO flight VALUES ('VS', 703, 10, 100, 0, 0, 5503.20, 550.99, 5503);
INSERT INTO airport_flight_origin VALUES ('VS', 703, 'LHR', '2019-11-27 1:30:30.00');
INSERT INTO airport_flight_destination VALUES ('VS', 703, 'ICN', '2019-11-27 2:30:30.00');


INSERT INTO booking VALUES ('AA', 400, 1, 1, 123456789123456, true);
INSERT INTO booking VALUES ('AA', 500, 1, 1, 123456789123456, true);
INSERT INTO booking VALUES ('KE', 300, 2, 2, 7654321987654321, false);