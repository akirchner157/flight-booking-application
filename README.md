# Flight Booking Application
#### Adrian Kirchner - A20425060
#### Alexander Bolejack - A20415099
### Some Notes:
- There is only sample data for five airports and 2 days (in UTC, once adjusted they may be earlier or later):
  - Airports:
    - ORD
    - LAX
    - ICN
    - LHR
    - DCA
  - Dates:
    - 2019-11-26 - 2019-11-27
- Credit Cards are linked to addresses and cannot be created without entering a valid address that already has been tied to the user
- The demo video is in .mkv format. If you cannot play this format, it is also available at https://youtu.be/LkD_6pfegK0